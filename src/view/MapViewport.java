package view;

import javafx.geometry.Rectangle2D;
import utils.Point;

public class MapViewport {
    private static double ZOOMING_CONST = 1.1;

    private double worldWidth;
    private double worldHeight;

    private double viewportWidth;
    private double viewportHeight;

    private Rectangle2D seenWorld;

    public MapViewport(double worldWidth, double worldHeight) {
        this.worldWidth = worldWidth;
        this.worldHeight = worldHeight;
        this.viewportWidth = 1;
        this.viewportHeight = 1;
        matchWorldToViewport();
    }

    public boolean intersectsTile(int x, int y) {
        return seenWorld.intersects(
                x, y, x + Main.TILE_SIZE, y + Main.TILE_SIZE);
    }

    public boolean zoomIn(Point<Double> viewportPosition) {
        Point<Double> worldPosition =
                getWorldPositionFromViewportPosition(viewportPosition);
        zoomHavingWorldPosition(worldPosition, new ZoomIn());
        return true;
    }

    public boolean zoomOut(Point<Double> viewportPosition) {
        Point<Double> worldPosition =
                getWorldPositionFromViewportPosition(viewportPosition);
        if (worldFitsInViewport()) {
            matchWorldToViewport();
            return false;
        }
        zoomHavingWorldPosition(worldPosition, new ZoomOut());
        if (worldFitsInViewport()) {
            matchWorldToViewport();
        }
        return true;
    }

    private void zoomHavingWorldPosition(Point<Double> worldPosition,
                                         ZoomDirection direction) {
        double minX = direction.scalingViewport(seenWorld.getMinX(), worldPosition.getX());
        double minY = direction.scalingViewport(seenWorld.getMinY(), worldPosition.getY());
        double maxX = direction.scalingViewport(seenWorld.getMaxX(), worldPosition.getX());
        double maxY = direction.scalingViewport(seenWorld.getMaxY(), worldPosition.getY());
        seenWorld = new Rectangle2D(minX, minY, maxX - minX, maxY - minY);
    }

    private static interface ZoomDirection {
        public Double scalingViewport(double viewport, double point);
    }

    private static class ZoomOut implements ZoomDirection {
        public Double scalingViewport(double viewport, double center) {
            return center + (viewport - center) * ZOOMING_CONST;
        }
    }

    private static class ZoomIn implements ZoomDirection {
        public Double scalingViewport(double viewport, double center) {
            return center + (viewport - center) / ZOOMING_CONST;
        }
    }

    public double getScale() {
        return (viewportWidth / seenWorld.getWidth());
    }

    public Point<Double> getWorldPositionFromViewportPosition(
            Point<Double> viewportPosition) {
        return new Point<Double>(
            seenWorld.getMinX() +
            seenWorld.getWidth() * (viewportPosition.getX() / viewportWidth),
            seenWorld.getMinY() +
            seenWorld.getHeight() * (viewportPosition.getY() / viewportHeight)
        );
    }

    public void setViewportSize(double viewportWidth, double viewportHeight) {
        this.viewportWidth = viewportWidth;
        this.viewportHeight = viewportHeight;
        matchWorldToViewport();
    }

    private boolean worldFitsInViewport() {
        return (seenWorld.getWidth() >= worldWidth
                && seenWorld.getHeight() >= worldHeight);
    }

    private void matchWorldToViewport() {
        if (worldWidth / worldHeight >=
            viewportWidth / viewportHeight) {
            matchByWidth();
        }
        else {
            matchByHeight();
        }
    }

    private void matchByWidth() {
        double scale = worldWidth / viewportWidth;
        this.seenWorld = new Rectangle2D(
                0,
                (worldHeight - viewportHeight * scale) / 2,
                worldWidth,
                viewportHeight * scale);
    }

    private void matchByHeight() {
        double scale = viewportHeight / worldHeight;
        this.seenWorld = new Rectangle2D(
                (worldWidth - worldWidth * scale) / 2,
                0,
                viewportWidth * scale,
                worldHeight);
    }

    private double getDownForToBigViewport() {
        return (worldHeight - seenWorld.getHeight()) / 2;
    }

    private double getLeftForToBigViewport() {
        return (worldWidth - seenWorld.getWidth()) / 2;
    }

    public Point<Double> getViewportPositionFromWorldPosition(
            Point<Double> worldPosition) {
        return new Point<Double>(
                (worldPosition.getX() - seenWorld.getMinX())
                        * (viewportWidth / seenWorld.getWidth()),
                (worldPosition.getY() - seenWorld.getMinY())
                        * (viewportHeight / seenWorld.getHeight())
        );
    }

    public double getSeenWidthRatio() {
        return Math.min(1., seenWorld.getWidth() / worldWidth);
    }

    public double getSeenHeightRatio() {
        return Math.min(1., seenWorld.getHeight() / worldHeight);
    }

    public double getHorizontalRatio() {
        if (worldWidth < seenWorld.getWidth()) {
            return 1;
        }
        if (seenWorld.getMinX() <= 0) {
            return 0;
        }
        return Math.min(1.,
                seenWorld.getMinX() / (worldWidth - seenWorld.getWidth()));
    }

    public double getVerticalRatio() {
        if (worldHeight < seenWorld.getHeight()) {
            return 1;
        }
        if (seenWorld.getMinY() <= 0) {
            return 0;
        }
        return Math.min(1.,
                seenWorld.getMinY() / (worldHeight - seenWorld.getHeight()));
    }

    public void setViewportLeftRatio(double left) {
        if (seenWorld.getWidth() >= worldWidth) {
            left = getLeftForToBigViewport();
        }
        else {
            left = worldWidth * left / 100;
        }
        seenWorld = new Rectangle2D(left, seenWorld.getMinY(),
                seenWorld.getWidth(), seenWorld.getHeight());
    }

    public void setViewportDownRatio(double down) {
        if (seenWorld.getHeight() >= worldHeight) {
            down = getDownForToBigViewport();
        }
        else {
            down = worldHeight * down / 100;
        }
        seenWorld = new Rectangle2D(seenWorld.getMinX(), down,
                seenWorld.getWidth(), seenWorld.getHeight());
    }
}
