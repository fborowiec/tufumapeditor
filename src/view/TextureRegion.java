package view;

import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import model.DeepCopiable;

public class TextureRegion implements DeepCopiable {
    private Image image;
    private String name;
    private Rectangle2D rect;

    public TextureRegion(final String name, final Image image, int x, int y, int width, int height) {
        this.name = name;
        this.image = image;
        this.rect = new Rectangle2D(x, y, width, height);
    }

    public void draw(Canvas canvas, int x, int y) {
        canvas.getGraphicsContext2D().drawImage(image, this.rect.getMinX(), this.rect.getMinY(),
                this.rect.getWidth(), this.rect.getHeight(), x, y, this.rect.getWidth(), this.rect.getHeight());
    }

    public void draw(Canvas canvas, int x, int y, int w, int h) {
        canvas.getGraphicsContext2D().drawImage(image, this.rect.getMinX(), this.rect.getMinY(),
                this.rect.getWidth(), this.rect.getHeight(), x, y, w, h);
    }

    public Rectangle2D getRectangle2D() {
        return this.rect;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public TextureRegion deepCopy() {
        return new TextureRegion(name, image, (int) rect.getMinX(),
                (int) rect.getMinY(), (int) rect.getWidth(), (int) rect.getHeight());
    }

    @Override
    public boolean equals(Object object) {
        if (! (object instanceof TextureRegion)) return false;
        TextureRegion that = (TextureRegion) object;
        if (! that.name.equals(this.name)) return false;
        if (! that.image.equals(this.image)) return false;
        if (that.rect.getMinX() != this.rect.getMinX()) return false;
        if (that.rect.getMinY() != this.rect.getMinY()) return false;
        if (that.rect.getWidth() != this.rect.getWidth()) return false;
        if (that.rect.getHeight() != this.rect.getHeight()) return false;
        return true;
    }
}
