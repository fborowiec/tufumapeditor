package view;

import controller.CellPanel;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class CellPanelDrawer {
    private Canvas canvas;
    private final CellPanel cellPanel;
    private final CellPanel.ClickHandler cellPanelClickHandler;
    private final ComboBox<String> comboBox;
    private ScrollPane textureScrollPane;
    private VBox whole;

    public CellPanelDrawer(final CellPanel cellPanel,
                           final CellPanel.ClickHandler cellPanelClickHandler) {
        this.cellPanelClickHandler = cellPanelClickHandler;
        this.cellPanel = cellPanel;
        this.canvas = new Canvas();
        this.canvas.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        defaultClickHandle(event);
                    }
                }
        );

        textureScrollPane = new ScrollPane();
        textureScrollPane.setContent(this.canvas);

        Pane pane = new Pane();
        pane.getChildren().add(textureScrollPane);

        comboBox = new ComboBox<String>();
        whole = new VBox();
        whole.getChildren().addAll(comboBox, pane);

        cellPanel.registerObserver(this);
        whole.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldVal, Number newVal) {
                textureScrollPane.setPrefWidth(newVal.doubleValue());
            }
        });

        whole.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldVal, Number newVal) {
                textureScrollPane.setPrefHeight(newVal.doubleValue() - comboBox.getHeight() - 23);
            }
        });
    }

    private void defaultClickHandle(MouseEvent event) {
        TextureRegion result = cellPanel.getTextureRegion((int) event.getX(), (int) event.getY());
        if (result != null) {
            System.out.println(result.getName());
            cellPanelClickHandler.handle(result);
        }
        else {
            System.out.println("null");
        }
    }
    public void invalidate() {
        canvas.setWidth(cellPanel.getCurrentImage().getWidth());
        canvas.setHeight(cellPanel.getCurrentImage().getHeight());
        ObservableList<String> items = FXCollections.observableArrayList(cellPanel.getImagesNames());
        comboBox.getItems().clear();
        comboBox.getItems().addAll(items);
        comboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                cellPanel.setCurrentImage(comboBox.getValue());
            }
        });
        draw();
    }

    public void draw() {
        this.canvas.getGraphicsContext2D().setFill(Color.WHITE);
        this.canvas.getGraphicsContext2D().fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        this.canvas.getGraphicsContext2D().drawImage(cellPanel.getImage(cellPanel.getName()), 0, 0);
    }

    public VBox getScrollPane() {
        return whole;
    }
}
