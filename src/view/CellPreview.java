package view;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class CellPreview {
    private TextureRegion cell;
    private Canvas canvas;
    private Pane pane;

    public CellPreview() {
        this.canvas = new Canvas(400, 400);
        this.pane = new Pane();
        pane.getChildren().add(this.canvas);
    }

    public void setCell(TextureRegion region) {
        this.cell = region;
        draw();
    }

    public Pane getWhole() {
        return pane;
    }

    private void draw() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, 400, 400);
        if (cell == null) return;
        cell.draw(canvas, 0, 0);
    }
}
