package view;

import javafx.scene.image.Image;

import java.util.Collection;
import java.util.Map;

public class TextureAtlas {
    private Map<String, Map<String, TextureRegion>> textures;
    private Map<String, Image> textureImages;

    public TextureAtlas(Map<String, Map<String, TextureRegion>> textures,
                        Map<String, Image> textureImages) {
        this.textures = textures;
        this.textureImages = textureImages;
    }

    public Collection<TextureRegion> getRegions(String imageName) {
        return textures.get(imageName).values();
    }

    public Image getImage(String name) {
        return textureImages.get(name);
    }

    public Map<String, Image> getImages() {
        return this.textureImages;
    }
}

