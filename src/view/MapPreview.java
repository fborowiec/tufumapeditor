package view;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import model.Cell;
import model.MapGrid;
import model.TiledLayer;
import utils.Point;

public class MapPreview {
    private MapViewport mapViewport;
    private ViewportScrollPane scrollPane;
    private Canvas canvas;
    private Pane scrollContent;
    private MapGrid mapGrid;

    public MapPreview() {
        canvas = new Canvas(0, 0);
        mapViewport = new MapViewport(0, 0);

        canvas.getGraphicsContext2D().setFill(Color.WHITE);

        scrollContent = new Pane();
        scrollContent.getChildren().add(canvas);

        scrollPane = new ViewportScrollPane(mapViewport, scrollContent, this);
    }

    public Point<Double> getWorldPositionFromClick(Point<Double> click) {
        return mapViewport.getWorldPositionFromViewportPosition(click);
    }

    public boolean zoomIn(Point<Double> point) {
        if (mapViewport.zoomIn(point)) {
            scrollPane.setBarsToViewport();
            draw();
            return true;
        }
        return false;
    }

    public boolean zoomOut(Point<Double> point) {
        if (mapViewport.zoomOut(point)) {
            scrollPane.setBarsToViewport();
            draw();
            return true;
        }
        return false;
    }

    public void setMapGrid(MapGrid mapGrid) {
        this.mapGrid = mapGrid;
        mapViewport = new MapViewport(mapGrid.getWidth(),
                                      mapGrid.getHeight());
        mapViewport.setViewportSize(
                scrollContent.getWidth(),
                scrollContent.getHeight());
        canvas = new Canvas(
                scrollContent.getWidth(),
                scrollContent.getHeight());
        scrollPane.setViewport(mapViewport);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        scrollContent.getChildren().clear();
        scrollContent.getChildren().add(canvas);
        draw();
    }

    public void showPreviewOnTileLayer(TextureRegion region,
                                       Point<Double> worldPosition) {
        double canvasX = worldPosition.getX();
        double canvasY = worldPosition.getY();

        Cell cell = mapGrid.getTiledLayer().get(worldPosition);
        draw();
        if (cell == null) {
            canvas.getGraphicsContext2D().setGlobalAlpha(0.5);
            drawTileHavingViewport(region,
                (Math.floor(canvasX / Main.TILE_SIZE) * Main.TILE_SIZE),
                (Math.floor(canvasY / Main.TILE_SIZE) * Main.TILE_SIZE)
            );
            canvas.getGraphicsContext2D().setGlobalAlpha(1.0);
        }
        else {
            canvas.getGraphicsContext2D().setGlobalAlpha(0.5);
            drawTileHavingViewport(region,
                    (Math.floor(canvasX / Main.TILE_SIZE) * Main.TILE_SIZE),
                    (Math.floor(canvasY / Main.TILE_SIZE) * Main.TILE_SIZE)
            );
            canvas.getGraphicsContext2D().setGlobalAlpha(1.0);
        }
    }

    public void showHighlightedOnPosition(Point<Double> worldPosition) {
        double x = worldPosition.getX();
        double y = worldPosition.getY();
        canvas.getGraphicsContext2D().setGlobalAlpha(0.5);
        draw();
        canvas.getGraphicsContext2D().setGlobalAlpha(1.0);
        mapGrid.getTiledLayer().get(worldPosition).getRegion().
                draw(canvas, (int) x, (int) y);
    }

    public void draw() {
        drawBackground();
        TiledLayer layer = mapGrid.getTiledLayer();
        Cell[][] cells = layer.getCells();
        for (int ix = 0; ix < layer.getWidth(); ix ++) {
            for (int iy = 0; iy < layer.getHeight(); iy ++) {
                if (cells[ix][iy] == null) continue;
                if (!mapViewport.intersectsTile(
                        ix * Main.TILE_SIZE, iy * Main.TILE_SIZE)) continue;
                drawTileHavingViewport(cells[ix][iy].getRegion(),
                        ix * Main.TILE_SIZE, iy * Main.TILE_SIZE);
            }
        }
    }

    private void drawTileHavingViewport(TextureRegion region,
                                        double x, double y) {
        Point<Double> corner = new Point<Double>(x, y);
        Point<Double> viewportPoint = mapViewport.
                getViewportPositionFromWorldPosition(corner);
        int viewportX = viewportPoint.getX().intValue();
        int viewportY = viewportPoint.getY().intValue();
        double size = mapViewport.getScale();
        region.draw(canvas, viewportX, viewportY,
                    (int) (size * region.getRectangle2D().getWidth()),
                    (int) (size * region.getRectangle2D().getHeight()));
    }

    private void drawBackground() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.RED);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        gc.setFill(Color.WHITE);
        Point<Double> viewportLeft = mapViewport.
                getViewportPositionFromWorldPosition(new Point<Double>(0., 0.));
        Point<Double> viewportRight = mapViewport.
                getViewportPositionFromWorldPosition(
                        new Point<Double>((double) mapGrid.getWidth(),
                                          (double) mapGrid.getHeight()));
        gc.fillRect(viewportLeft.getX(), viewportLeft.getY(),
                viewportRight.getX() - viewportLeft.getX(),
                viewportRight.getY() - viewportLeft.getY());
    }

    public Pane getContent() {
        return scrollContent;
    }

    public Pane getWhole() {
        return scrollPane.getWhole();
    }
}
