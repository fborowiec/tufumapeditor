package view;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Control;
import javafx.scene.control.ScrollBar;
import javafx.scene.layout.*;

public class ViewportScrollPane extends Control {
    private ScrollBar horizontalBar;
    private ScrollBar verticalBar;
    private GridPane gridPane;
    private MapViewport mapViewport;
    private MapPreview preview;

    public ViewportScrollPane(final MapViewport mapViewport,
                              final Pane content, final MapPreview preview) {
        super();
        this.preview = preview;
        this.mapViewport = mapViewport;
        this.horizontalBar = new ScrollBar();
        this.verticalBar = new ScrollBar();
        this.horizontalBar.setOrientation(Orientation.HORIZONTAL);
        this.verticalBar.setOrientation(Orientation.VERTICAL);

        this.gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(0);
        gridPane.setVgap(0);
        gridPane.setPadding(new Insets(0, 0, 0, 0));
        ColumnConstraints cc1 = new ColumnConstraints();
        cc1.setHgrow(Priority.ALWAYS);

        RowConstraints rc1 = new RowConstraints();
        rc1.setVgrow(Priority.ALWAYS);
        gridPane.getColumnConstraints().add(cc1);
        gridPane.getRowConstraints().add(rc1);
        gridPane.add(content, 0, 0);
        gridPane.add(horizontalBar, 0, 1);
        gridPane.add(verticalBar, 1, 0);
        this.gridPane.setGridLinesVisible(true);

        horizontalBar.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue,
                                Number oldValue, Number newValue) {
                updateHorizontal();
                redraw();
            }
        });

        verticalBar.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue,
                                Number oldValue, Number newValue) {
                updateVertical();
                redraw();
            }
        });
    }

    private void updateHorizontal() {
        mapViewport.setViewportLeftRatio(
                horizontalBar.getValue() * (1 - horizontalBar.getVisibleAmount() / 100));
    }

    private void updateVertical() {
        mapViewport.setViewportDownRatio(
                verticalBar.getValue() * (1 - verticalBar.getVisibleAmount() / 100));
    }

    private void redraw() {
        preview.draw();
    }
    public void setBarsToViewport() {
        horizontalBar.setVisibleAmount(mapViewport.getSeenWidthRatio() * 100);
        verticalBar.setVisibleAmount(mapViewport.getSeenHeightRatio() * 100);
        horizontalBar.setValue(mapViewport.getHorizontalRatio() * 100);
        verticalBar.setValue(mapViewport.getVerticalRatio() * 100);
        updateHorizontal();
        updateVertical();
    }

    public Pane getWhole() {
        return this.gridPane;
    }

    public void setViewport(MapViewport viewport) {
        this.mapViewport = viewport;
    }
}

