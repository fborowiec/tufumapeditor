package view;

import controller.CellPanel;
import controller.MapController;
import controller.UserState;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.*;
import javafx.stage.Screen;
import javafx.stage.Stage;
import menu.MenuFactory;


public class Main extends Application {
    public static final int TILE_SIZE = 70;

    private MapController mapController;
    private CellPanel cellPanel;

    // UI components
    private Stage primaryStage;
    private GridPane grid;
    private MenuBar menuBar;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        initGridPaneLayout();
        initMenuBar();
        initBorderPaneLayout();
    }

    private void initMenuBar() {
        // Initializing menu bar
        menuBar = new MenuBar();
        menuBar.getMenus().addAll(MenuFactory.getMenus(mapController,
                primaryStage, cellPanel));
    }

    private void initBorderPaneLayout() {
        BorderPane borderPane = new BorderPane();
        borderPane.setTop(menuBar);
        borderPane.setCenter(grid);

        primaryStage.setResizable(true);

        primaryStage.setMaxHeight(Screen.getPrimary().getVisualBounds().getHeight());
        primaryStage.setMaxWidth(Screen.getPrimary().getVisualBounds().getWidth());

        // Resize handling
        primaryStage.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldVal, Number newVal) {
                invalidate();
            }
        });

        primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number oldVal, Number newVal) {
                invalidate();
            }
        });

        primaryStage.setTitle("Tufu Map Editor");
        primaryStage.show();
        primaryStage.setScene(new Scene(borderPane));
    }

    private void initGridPaneLayout() {
        // Creating views
        MapPreview mapPreview = new MapPreview();
        CellPreview cellPreview = new CellPreview();

        // Creating controllers
        final UserState userState = new UserState();
        cellPanel = new CellPanel(null, userState);
        final CellPanel.ClickHandler cellPanelClickHandler = cellPanel.new ClickHandler(cellPreview);
        final CellPanelDrawer cellPanelDrawer = new CellPanelDrawer(cellPanel, cellPanelClickHandler);
        mapController = new MapController(mapPreview, userState);

        // Positioning views
        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(0);
        grid.setVgap(0);
        grid.setPadding(new Insets(0, 0, 0, 0));


        ColumnConstraints c = new ColumnConstraints();
        c.setHgrow(Priority.ALWAYS);
        c.setPercentWidth(65);
        grid.getColumnConstraints().add(c);

        RowConstraints r = new RowConstraints();
        r.setVgrow(Priority.ALWAYS);
        r.setPercentHeight(60);
        grid.getRowConstraints().add(r);

        grid.add(mapPreview.getWhole(), 0, 0, 1, 2);
        grid.add(cellPanelDrawer.getScrollPane(), 1, 0, 1, 1);
        grid.add(cellPreview.getWhole(), 1, 1, 1, 1);

        // For debug purposes
        // grid.setGridLinesVisible(true);
        //cellPanelDrawer.draw();
    }

    private void invalidate() {
        double width = primaryStage.widthProperty().get();
        double height = primaryStage.heightProperty().get();
        grid.resize(width, height);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
