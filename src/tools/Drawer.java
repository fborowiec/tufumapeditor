package tools;

import controller.UserState;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import model.Layer;

public interface Drawer {
    public boolean mouseClicked(final UserState userState, final MouseEvent event);
    public boolean mouseDragged(final UserState userState, final MouseEvent event);
    public boolean mouseMoved(final UserState userState, final MouseEvent event);
    public boolean mouseExited(final UserState userState, final MouseEvent event);
    public boolean scrolled(final UserState userState, final ScrollEvent event);
}
