package controller;

import view.TextureRegion;

public class UserState {
    private TextureRegion region;

    public TextureRegion getChosenRegion() {
        return region;
    }

    public void setChosenRegion(TextureRegion region) {
        this.region = region;
    }
}
