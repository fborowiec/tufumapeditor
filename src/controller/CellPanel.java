package controller;

import javafx.scene.image.Image;
import view.CellPanelDrawer;
import view.CellPreview;
import view.TextureAtlas;
import view.TextureRegion;

import java.util.*;

public class CellPanel {
    public class ClickHandler {
        private CellPreview preview;
        public ClickHandler(final CellPreview preview) {
            this.preview = preview;
        }

        public void handle(TextureRegion region) {
            this.preview.setCell(region);
            userState.setChosenRegion(region);
        }
    }

    private final UserState userState;
    private TextureAtlas atlas;
    private String currentImageName;
    private Set<TextureRegion> rectangles;
    private Set<CellPanelDrawer> observers;

    public CellPanel(TextureAtlas atlas, final UserState userState) {
        this.userState = userState;
        //setTextureAtlas(atlas);
        observers = new HashSet<CellPanelDrawer>();
    }

    private void updateView() {
        rectangles.clear();
        for (TextureRegion region: this.atlas.getRegions(currentImageName)) {
            rectangles.add(region);
        }
    }

    public TextureRegion getTextureRegion(int x, int y) {
        for (TextureRegion region: rectangles) {
            if (region.getRectangle2D().contains(x, y)) {
                return region;
            }
        }
        return null;
    }

    public void setTextureAtlas(TextureAtlas textureAtlas) {
        System.out.println("setTextureAtlas");
        this.atlas = textureAtlas;
        rectangles = new HashSet<TextureRegion>();
        currentImageName = (new ArrayList<String>(atlas.getImages().keySet())).get(0);
        updateView();
        invalidateView();
    }

    public String getName() {
        return this.currentImageName;
    }

    public Image getImage(String name) {
        if (atlas == null) return null;
        return atlas.getImage(name);
    }

    public Collection<String> getImagesNames() {
        if (atlas == null) return null;
        return atlas.getImages().keySet();
    }

    public void setCurrentImage(String name) {
        this.currentImageName = name;
        updateView();
        invalidateView();
    }

    public Image getCurrentImage() {
        return atlas.getImage(currentImageName);
    }

    private void invalidateView() {
        for (CellPanelDrawer drawer: observers) {
            drawer.invalidate();
        }
    }

    public void registerObserver(CellPanelDrawer drawer) {
        observers.add(drawer);
    }
}
