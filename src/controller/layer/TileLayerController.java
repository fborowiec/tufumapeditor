package controller.layer;

import controller.UserState;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import model.Cell;
import model.MapGrid;
import model.SimpleCell;
import model.TiledLayer;
import tools.Drawer;
import utils.Point;
import view.MapPreview;

import java.util.*;

public class TileLayerController implements LayerController {
    private final MapPreview mapPreview;
    private final MapGrid mapGrid;
    private Drawer currentDrawer;
    private Map<String, Drawer> drawerMap;

    public TileLayerController(final MapGrid mapGrid,
                               final MapPreview mapPreview) {
        this.mapPreview = mapPreview;
        this.mapGrid = mapGrid;
        this.drawerMap = new HashMap<String, Drawer>();
        this.drawerMap.put("Brush", new Brush());
        this.drawerMap.put("Picker", new Picker());
        this.currentDrawer = drawerMap.get("Brush");
    }

    @Override
    public Collection<String> getDrawerNames(){
        List<String> result = new ArrayList<String>();
        for (String name: drawerMap.keySet()) {
            result.add(name);
        }
        return result;
    }

    @Override
    public void setDrawerByName(String name) {
        this.currentDrawer = drawerMap.get(name);
    }

    @Override
    public boolean mouseClicked(UserState userState, MouseEvent event) {
        boolean handled = currentDrawer.mouseClicked(userState, event);
        return handled;
    }

    @Override
    public boolean mouseDragged(UserState userState, MouseEvent event) {
        boolean handled = currentDrawer.mouseDragged(userState, event);
        return handled;
    }

    @Override
    public boolean mouseMoved(UserState userState, MouseEvent event) {
        boolean handled = currentDrawer.mouseMoved(userState, event);
        return handled;
    }

    @Override
    public boolean mouseExited(UserState userState, MouseEvent event) {
        boolean handled = currentDrawer.mouseExited(userState, event);
        return handled;
    }

    @Override
    public boolean scrolled(UserState userState, ScrollEvent event) {
        boolean handled = currentDrawer.scrolled(userState, event);
        return handled;
    }

    private class Brush implements Drawer {

        private Brush() {
        }

        @Override
        public boolean mouseClicked(final UserState userState, MouseEvent mouseEvent) {
            Point<Double> click = new Point<Double>(mouseEvent.getX(),
                                                    mouseEvent.getY());
            Point<Double> worldPosition = mapPreview.
                    getWorldPositionFromClick(click);
            TiledLayer layer = mapGrid.getTiledLayer();

            if ((userState.getChosenRegion() != null
                    && (layer.get(worldPosition) == null)
                || (userState.getChosenRegion()
                    != layer.get(worldPosition).getRegion()))) {

                layer.set(new SimpleCell(userState.getChosenRegion()),
                        worldPosition);
                mapPreview.draw();
                return true;
            }
            return false;
        }

        @Override
        public boolean mouseDragged(final UserState userState, MouseEvent event) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return mouseClicked(userState, event);
        }

        @Override
        public boolean mouseMoved(final UserState userState, MouseEvent event) {
            Point<Double> click = new Point<Double>(event.getX(),
                                                    event.getY());
            Point<Double> worldPoint = mapPreview.
                    getWorldPositionFromClick(click);
            mapPreview.draw();

            if (userState.getChosenRegion() != null) {
                mapPreview.showPreviewOnTileLayer(userState.getChosenRegion(),
                                                  worldPoint);
                return true;
            }
            return false;
        }

        @Override
        public boolean mouseExited(final UserState userState, MouseEvent mouseEvent) {
            mapPreview.draw();
            return true;
        }

        @Override
        public boolean scrolled(final UserState userState, ScrollEvent event) {
            return false;
        }
    }

    private class Picker implements Drawer {
        private Cell pickedCell;

        private Picker() {
            this.pickedCell = null;
        }

        @Override
        public boolean mouseClicked(UserState userState, MouseEvent event) {
            Point<Double> click = new Point<Double>(event.getX(), event.getY());
            Point<Double> worldPosition = mapPreview.
                    getWorldPositionFromClick(click);

            Cell pickedCellCandidate = mapGrid.getTiledLayer().
                                               get(worldPosition);

            mapPreview.draw();
            if (pickedCellCandidate != null) {
                pickedCell = pickedCellCandidate;
                mapPreview.showHighlightedOnPosition(worldPosition);
            }
            return true;
        }

        @Override
        public boolean mouseDragged(UserState userState, MouseEvent event) {
            return false;
        }

        @Override
        public boolean mouseMoved(UserState userState, MouseEvent event) {
            return false;
        }

        @Override
        public boolean mouseExited(UserState userState, MouseEvent event) {
            return false;
        }

        @Override
        public boolean scrolled(UserState userState, ScrollEvent event) {
            return false;
        }
    }
}
