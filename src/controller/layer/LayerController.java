package controller.layer;

import tools.Drawer;

import java.util.Collection;

public interface LayerController extends Drawer {
    public Collection<String> getDrawerNames();
    public void setDrawerByName(String name);
}
