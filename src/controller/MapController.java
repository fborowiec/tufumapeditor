package controller;

import controller.layer.LayerController;
import controller.layer.TileLayerController;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import model.MapGrid;
import utils.Point;
import view.MapPreview;

import java.util.ArrayDeque;
import java.util.Deque;

public class MapController {
    public static final int MAX_SAVED_STATES = 100;

    private MapGrid mapGrid;
    private final MapPreview mapPreview;
    private final UserState userState;
    private LayerController currentLayerController;

    // For undo support
    private Deque<MapGrid.Memento> savedStates;

    public MapController(final MapPreview mapPreview, final UserState userState) {
        this.userState = userState;
        this.mapPreview = mapPreview;

        this.savedStates = new ArrayDeque<MapGrid.Memento>();

        mapPreview.getContent().addEventFilter(ScrollEvent.ANY, new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                defaultMouseScrolledHandle(event);
            }
        });

        mapPreview.getContent().addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                defaultMouseClickedHandle(mouseEvent);
            }
        });

        mapPreview.getContent().addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                defaultMouseDraggedHandle(mouseEvent);
            }
        });

        mapPreview.getContent().addEventFilter(MouseEvent.MOUSE_MOVED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                defaultMouseMovedHandle(mouseEvent);
            }
        });

        mapPreview.getWhole().addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                defaultMouseExitedHandle(mouseEvent);
            }
        });
    }

    private void defaultMouseScrolledHandle(ScrollEvent event) {
        if (currentLayerController == null) return;
        boolean handled = currentLayerController.scrolled(userState, event);
        if (handled) {
            saveState();
            return;
        }
        Point<Double> eventPoint = new Point<Double>(event.getX(),
                                                     event.getY());
        if (event.getDeltaY() > 0) {
            mapPreview.zoomIn(eventPoint);
        } else if (event.getDeltaY() < 0) {
            mapPreview.zoomOut(eventPoint);
        }
        event.consume();
    }

    private void defaultMouseClickedHandle(MouseEvent mouseEvent) {
        if (currentLayerController != null
                && currentLayerController.mouseClicked(userState, mouseEvent))
            saveState();
    }

    private void defaultMouseDraggedHandle(MouseEvent mouseEvent) {
        if (currentLayerController != null
                && currentLayerController.mouseDragged(userState, mouseEvent))
            saveState();
    }

    private void defaultMouseMovedHandle(MouseEvent mouseEvent) {
        if (currentLayerController != null) {
            currentLayerController.mouseMoved(userState, mouseEvent);
        }
    }

    private void defaultMouseExitedHandle(MouseEvent mouseEvent) {
        if (currentLayerController != null) {
            currentLayerController.mouseExited(userState, mouseEvent);
        }
    }

    public boolean undo() {
        if (savedStates.isEmpty()) {
            throw new RuntimeException("Empty saved states stack");
        }
        else {
            if (savedStates.size() > 1) savedStates.pollFirst();
            mapGrid = savedStates.peekFirst().getSavedState();
        }
        mapPreview.setMapGrid(mapGrid);
        mapPreview.draw();
        return true;
    }

    public void setMapGrid(MapGrid mapGrid) {
        this.mapGrid = mapGrid;
        mapPreview.setMapGrid(mapGrid);
        this.currentLayerController = new TileLayerController(mapGrid,
                                                              mapPreview);
        savedStates.clear();
        saveState();
    }

    private void saveState() {
        if (savedStates.size() >= MAX_SAVED_STATES) {
            savedStates.pollLast();
        }
        savedStates.addFirst(new MapGrid.Memento(mapGrid));
    }
}
