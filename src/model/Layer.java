package model;

import utils.Point;

public interface Layer {
    public Cell get(Point<Double> worldPosition);
    public void set(Cell cell, Point<Double> worldPosition);
    public Layer deepCopy();
}
