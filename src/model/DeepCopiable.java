package model;

public interface DeepCopiable {
    public DeepCopiable deepCopy();
}
