package model;

import view.Main;

public class MapGrid {
    private TiledLayer tiledLayer;
    private int height;
    private int width;

    public MapGrid(int width, int height) {
        this.width = width;
        this.height = height;
        this.tiledLayer = new TiledLayer(this.width / Main.TILE_SIZE,
                                         this.height / Main.TILE_SIZE);
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public TiledLayer getTiledLayer() {
        return this.tiledLayer;
    }

    public static class Memento {
        private final MapGrid state;

        public Memento(MapGrid toSave) {
            state = new MapGrid(toSave.width, toSave.height);
            state.tiledLayer = toSave.tiledLayer.deepCopy();
        }

        public MapGrid getSavedState() {
            Memento newMemento = new Memento(state);
            return newMemento.state;
        }
    }
}
