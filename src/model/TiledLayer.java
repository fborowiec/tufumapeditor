package model;

import utils.Point;
import view.Main;

public class TiledLayer implements Layer {
    private int width;
    private int height;
    private Cell[][] cells;

    public TiledLayer(int width, int height) {
        this.width = width;
        this.height = height;
        cells = new Cell[width][height];
    }

    public Cell get(Point<Double> worldPosition) {
        double x = worldPosition.getX();
        double y = worldPosition.getY();
        int rx = (int) Math.floor(x / Main.TILE_SIZE);
        int ry = (int) Math.floor(y / Main.TILE_SIZE);
        if (rx >= cells.length || ry >= cells[0].length) return null;
        if (rx < 0 || ry < 0) return null;
        return cells[rx][ry];
    }

    @Override
    public void set(Cell cell, Point<Double> worldPosition) {
        double x = worldPosition.getX();
        double y = worldPosition.getY();
        int rx = (int) Math.floor(x / Main.TILE_SIZE);
        int ry = (int) Math.floor(y / Main.TILE_SIZE);
        if (rx >= cells.length || ry >= cells[0].length) return;
        if (rx < 0 || ry < 0) return;
        cells[rx][ry] = cell;
    }

    @Override
    public TiledLayer deepCopy() {
        TiledLayer result = new TiledLayer(width, height);
        for (int x = 0; x < cells.length; ++ x) {
            for (int y = 0; y < cells[0].length; ++ y) {
                result.cells[x][y] = cells[x][y] == null ? null : cells[x][y].deepCopy();
            }
        }
        return result;
    }

    public Cell[][] getCells() {
        return this.cells;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }
}

