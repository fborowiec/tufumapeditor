package model;

import view.TextureRegion;

public class SimpleCell extends Cell {

    public SimpleCell(TextureRegion region){
        super(region);
    }

    @Override
    public SimpleCell deepCopy() {
        return new SimpleCell(region.deepCopy());
    }

    @Override
    public boolean equals(Object object) {
        if (! (object instanceof SimpleCell)) return false;
        return ((SimpleCell) object).region.equals(this.region);
    }
}
