package model;

import view.TextureRegion;

public abstract class Cell implements DeepCopiable {
    protected TextureRegion region;

    public Cell(TextureRegion region) {
        this.region = region;
    }

    public TextureRegion getRegion() {
        return this.region;
    }

    @Override
    public abstract Cell deepCopy();
}
