package menu;

import model.MapGrid;

public class MapGridCreation {
    public static final int WIDTH_DEFAULT = 70 * 20;
    public static final int HEIGHT_DEFAULT = 70 * 5;

    private int width;
    private int height;

    public MapGridCreation() {
        width = WIDTH_DEFAULT;
        height = HEIGHT_DEFAULT;
    }

    public boolean isReadyToCreate() {
        return true;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public MapGrid create() {
        return new MapGrid(width, height);
    }
}
