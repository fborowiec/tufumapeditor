package menu;

import javafx.scene.image.Image;
import view.TextureAtlas;
import view.TextureRegion;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class AtlasReader {
    private enum FileState {
        ALL_DONE, ATLAS_FINISHED, CONTINUE
    }
    private Map<String, Map<String, TextureRegion>> textures;
    private Map<String, Image> textureImages;
    private String path;

    //for example 'my/drawings', 'sample.atlas'
    public AtlasReader(File atlasFile) {
        this.path = atlasFile.getParent();
        String atlas = atlasFile.getName();

        textures = new HashMap<String, Map<String, TextureRegion>>();
        textureImages = new HashMap<String, Image>();
        StringBuilder builder = new StringBuilder();

        builder.append(path);
        builder.append(File.separator);
        builder.append(atlas);
        try {
            loadAtlas(builder.toString());
        } catch (IOException e) {
            System.out.println("Unable to read atlas description");
            e.printStackTrace();
        }
    }

    public TextureAtlas getTextureAtlas() {
        return new TextureAtlas(textures, textureImages);
    }

    private FileState readTextureRegion(final BufferedReader br,
                                        Image image,
                                        String imageName) throws IOException {
        String name = br.readLine();
        if (name == null) {
            return FileState.ALL_DONE;
        }
        if (name.equals("")) {
            return FileState.ATLAS_FINISHED;
        }
        String rotateString = br.readLine();
        String xyString = br.readLine();
        String sizeString = br.readLine();
        String origString = br.readLine();
        String offsetString = br.readLine();
        String indexString = br.readLine();

        String regex = "\\s+[\\D*\\:]*\\s*|,\\s";
        String[] parts = xyString.split(regex);
        int x = Integer.valueOf(parts[1]);
        int y = Integer.valueOf(parts[2]);

        parts = sizeString.split(regex);
        int width = Integer.valueOf(parts[1]);
        int height = Integer.valueOf(parts[2]);
        String realName = path + File.separator + name;
        if (!textures.containsKey(imageName)) {
            textures.put(imageName, new HashMap<String, TextureRegion>());
        }
        textures.get(imageName).put(realName, new TextureRegion(
                realName, image, x, y, width, height));
        return FileState.CONTINUE;
    }

    private void loadAtlas(String path) throws IOException {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(fis));

        while (true) {
            String pngName = br.readLine();
            if (pngName.equals("")) pngName = br.readLine();
            String sizeString = br.readLine(); // 'size: 1022,1022
            String formatString = br.readLine(); // 'format: RGBA8888'
            String filterString = br.readLine(); // 'filter: Nearest, Nearest
            String repeatString = br.readLine(); // 'repeat: none'

            String fullImageName = this.path + File.separator + pngName;
            Image image = new Image("file:" + fullImageName);
            textureImages.put(fullImageName, image);

            FileState state;
            while ((state = readTextureRegion(br, image, fullImageName)) == FileState.CONTINUE) {}
            if (state == FileState.ALL_DONE) return;
        }
    }
}
