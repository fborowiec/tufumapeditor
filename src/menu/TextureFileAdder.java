package menu;

import javafx.stage.FileChooser;
import javafx.stage.Window;
import view.TextureAtlas;

import java.io.File;

public class TextureFileAdder {
    public static TextureAtlas getAtlasFile(Window ownerWindow) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose atlas file");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Atlas files",
                                                "*.atlas")
        );

        File atlasFile = fileChooser.showOpenDialog(ownerWindow);
        if (atlasFile == null) {
            return null;
        }

        AtlasReader ar = new AtlasReader(atlasFile);
        return ar.getTextureAtlas();
    }
}
