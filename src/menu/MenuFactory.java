package menu;

import controller.CellPanel;
import controller.MapController;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import view.TextureAtlas;

import java.util.ArrayList;
import java.util.List;

public class MenuFactory {
    public static List<Menu> getMenus(final MapController mapController,
                                      final Stage stage,
                                      final CellPanel cellPanel) {
        ArrayList<Menu> res = new ArrayList<Menu>();
        res.add(createAndGetFileMenu(mapController, stage, cellPanel));
        res.add(createAndGetEditMenu(mapController));
        res.add(createAndGetToolsMenu(mapController));
        return res;
    }

    private static Menu createAndGetFileMenu(
            final MapController mapController, final Stage stage,
            final CellPanel cellPanel) {
        Menu fileMenu = new Menu("File");
        MenuItem newMapCmd = new MenuItem("New Map");
        MenuItem saveMapCmd = new MenuItem("Save");
        MenuItem loadAtlasCmd = new MenuItem("Load atlas");
        MenuItem exitCmd = new MenuItem("Exit");

        newMapCmd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                MapGridCreationMenu.createAndGet(mapController).show(stage);
            }
        });

        saveMapCmd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                throw new RuntimeException("Implement saving!");
            }
        });

        loadAtlasCmd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                TextureAtlas ta = TextureFileAdder.getAtlasFile(stage.getOwner());
                if (ta != null) {
                    cellPanel.setTextureAtlas(ta);
                }
            }
        });

        newMapCmd.setAccelerator(new KeyCodeCombination(
                KeyCode.N, KeyCombination.CONTROL_DOWN));
        saveMapCmd.setAccelerator(new KeyCodeCombination(
                KeyCode.S, KeyCombination.CONTROL_DOWN));
        loadAtlasCmd.setAccelerator(new KeyCodeCombination(
                KeyCode.A, KeyCombination.CONTROL_DOWN));
        exitCmd.setAccelerator(new KeyCodeCombination(
                KeyCode.Q, KeyCombination.CONTROL_DOWN));

        fileMenu.getItems().addAll(newMapCmd, saveMapCmd,
                                   loadAtlasCmd, exitCmd);
        return fileMenu;
    }

    private static Menu createAndGetEditMenu(
            final MapController mapController) {
        Menu editMenu = new Menu("Edit");
        MenuItem undoCmd = new MenuItem("Undo");

        undoCmd.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                mapController.undo();
            }
        });

        undoCmd.setAccelerator(new KeyCodeCombination(
                KeyCode.Z, KeyCombination.CONTROL_DOWN));

        editMenu.getItems().addAll(undoCmd);
        return editMenu;
    }

    private static Menu createAndGetToolsMenu(
            final MapController mapController) {
        Menu toolsMenu = new Menu("Tools");
        MenuItem brush = new MenuItem("Brush");
        MenuItem picker = new MenuItem("Picker");

/*
        brush.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                mapController.setDrawer(new Brush(mapController));
            }
        });

        picker.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                mapController.setDrawer(new Picker(mapController));
            }
        });
        */
        toolsMenu.getItems().addAll(brush, picker);
        return toolsMenu;
    }
}
