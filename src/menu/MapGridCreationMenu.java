package menu;

import controller.MapController;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Popup;
import view.Main;

public class MapGridCreationMenu {
    public static Popup createAndGet(final MapController mapController) {
        final MapGridCreation mapGridCreation = new MapGridCreation();
        ScrollPane pane = new ScrollPane();
        GridPane gridPane = new GridPane();
        //Make menu in gridPane

        // Adding width and height
        Label width = new Label("Width:");
        Label height = new Label("Height:");

        final TextField widthField = new TextField();
        final TextField heightField = new TextField();
        widthField.setPrefColumnCount(6);
        heightField.setPrefColumnCount(6);
        HBox widthBox = new HBox();
        HBox heightBox = new HBox();

        widthBox.setSpacing(10);
        heightBox.setSpacing(10);

        widthBox.getChildren().addAll(width, widthField);
        heightBox.getChildren().addAll(height, heightField);

        Button button = new Button("Create");

        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.add(widthBox, 0, 0);
        gridPane.add(heightBox, 0, 1);
        gridPane.add(button, 0, 2);

        // Return results
        pane.setContent(gridPane);
        pane.setPrefViewportWidth(150);
        pane.setPrefViewportHeight(300);
        final Popup popup = new Popup();
        popup.getContent().add(pane);

        button.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                mapGridCreation.setHeight(Integer.valueOf(heightField.getText()));
                mapGridCreation.setWidth(Integer.valueOf(widthField.getText()));
                if (mapGridCreation.isReadyToCreate()) {
                    mapController.setMapGrid(mapGridCreation.create());
                    popup.hide();
                }
                else {
                    // TODO give user feedback
                    return;
                }
            }
        });
        return popup;
    }
}
