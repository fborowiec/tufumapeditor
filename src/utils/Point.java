package utils;

public class Point<E> {
    E x;
    E y;

    public Point(E x, E y) {
        this.x = x;
        this.y = y;
    }

    public void set(E x, E y) {
        this.x = x;
        this.y = y;
    }

    public E getX() {
        return x;
    }

    public E getY() {
        return y;
    }
}
